﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4_2
{
    public abstract class Make_abs
    {
        public void make()
        {
            choseParametres();
            loadingIngredients();
            Shaping();
            Packaging();

        }
        public void loadingIngredients()
        {
            Console.WriteLine("All ingredients loaded");
        }
        public abstract void choseParametres();
        public abstract void Shaping();
        public abstract void Packaging();


    }
    public class Make_mod1 : Make_abs
    {
        public override void choseParametres()
        {
            Console.WriteLine("Ingredients: milky cheese, dried apricots");
        }

        public override void Shaping()
        {
            Console.WriteLine("Quick shaping for best result, more puree");
        }

        public override void Packaging()
        {
            Console.WriteLine("Best foil chosen");

        }
        bool clientHasMoney() { return false; }
    }
    public class Make_mod2 : Make_abs
    {
        public override void choseParametres()
        {
            Console.WriteLine("Ingredients: milky cheese, raisins");
        }

        public override void Shaping()
        {
            Console.WriteLine("Quick shaping for best result, more puree");
        }

        public override void Packaging()
        {
            Console.WriteLine("Best paper chosen");

        }
    }

    public class Make_mod3 : Make_abs
    {
        public override void choseParametres()
        {
            Console.WriteLine("Ingredients: chokalade milky cheese, raisins");
        }

        public override void Shaping()
        {
            Console.WriteLine("Quick shaping for best result, more puree");
        }

        public override void Packaging()
        {
            Console.WriteLine("Best plastick chosen");

        }
    }

    class Program{
        static void Main(string[] args)
        {
            Make_abs s1 = new Make_mod1();
            s1.make();
            Make_abs s2 = new Make_mod2();
            s2.make();
            Make_abs s33 = new Make_mod3();
            s33.make();
        }
    }
    
}
