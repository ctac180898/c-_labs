﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3_2
{
    class MainApp
    {
        public static void Main()
        {
            // Create and run the African animal kiosk
            CocktaileMachine alcohol = new AlcoholicCocktaileMachine();
            Kiosk kiosk = new Kiosk(alcohol);
            kiosk.PourCocktaile();
            // Create and run the American animal kiosk
            CocktaileMachine nonAlcohol = new NonAlcoholicCocktaileMachine();
            kiosk = new Kiosk(nonAlcohol);
            kiosk.PourCocktaile();
            // Wait for user input
            Console.ReadKey();
        }
    }
    /// <summary>
    /// ///////
    /// </summary>
// The 'AbstractFactory' abstract class
abstract class CocktaileMachine
    {
        public abstract Mojito CreateMojito();
        public abstract PinaColada CreatePinaColada();
    }
    // The 'ConcreteFactory1' class
    class AlcoholicCocktaileMachine : CocktaileMachine
    {
        public override Mojito CreateMojito()
        {
            return new AlcoholicMojito();
        }
        public override PinaColada CreatePinaColada()
        {
            return new AlcoholicPinaColada();
        }
    }
    // The 'ConcreteFactory2' class
    class NonAlcoholicCocktaileMachine : CocktaileMachine
    {
        public override Mojito CreateMojito()
        {
            return new NonAlcoholicMojito();
        }
        public override PinaColada CreatePinaColada()
        {
            return new NonAlcoholicPinaColada();
        }
    }
    // The 'AbstractProductA' abstract class
    abstract class Mojito
    {
        public abstract void Pour(int coctailInt, int ageInt);
    }
    // The 'AbstractProductB' abstract class
    abstract class PinaColada
    {
        public abstract void Pour(int coctailInt, int ageInt);
    }
    // The 'ProductA1' class
    class AlcoholicMojito : Mojito
    {
        public override void Pour(int coctailInt, int ageInt)
        {

            Console.WriteLine("You are " + ageInt + " years old, so you have " + this.GetType().Name);


        }
    }
    // The 'ProductB1' class
    class NonAlcoholicPinaColada : PinaColada
    {
        public override void Pour(int coctailInt, int ageInt)
        {

            Console.WriteLine("You are " + ageInt + " years old, so you have " + this.GetType().Name);


        }
    }
    // The 'ProductA2' class
    class AlcoholicPinaColada : PinaColada
    {
        public override void Pour(int coctailInt, int ageInt)
        {

            Console.WriteLine("You are " + ageInt + " years old, so you have " + this.GetType().Name);


        }
    }
    // The 'ProductB2' class
    class NonAlcoholicMojito : Mojito
    {
        public override void Pour(int coctailInt, int ageInt)
        {

            Console.WriteLine("You are " + ageInt + " years old, so you have " + this.GetType().Name);


        }
    }

// The 'Client' class
class Kiosk
    {
        private Mojito _mojito1;
        private Mojito _mojito2;
        private PinaColada _pinacolada1;
        private PinaColada _pinacolada2;
        // Constructor
        public Kiosk(CocktaileMachine factory)
        {
            if(factory.GetType().Name == "NonAlcoholicCocktaileMachine")
            {
                _mojito1 = factory.CreateMojito();
                _pinacolada1 = factory.CreatePinaColada();
            }


            else if (factory.GetType().Name == "AlcoholicCocktaileMachine")
            {
                _mojito2 = factory.CreateMojito();
                _pinacolada2 = factory.CreatePinaColada();
            }
                
        }
        public void PourCocktaile()
        {
            Console.WriteLine("Type your age");
            string age = Console.ReadLine();
            int ageInt = Int32.Parse(age);
           // Console.WriteLine(ageInt);

            Console.WriteLine("Type coctail number (1 or 2)");
            string coctail = Console.ReadLine();
            int coctailInt = Int32.Parse(coctail);
            //Console.WriteLine(coctailInt);


            if (coctailInt == 1)
            {
                if (ageInt >= 18)
                    _mojito2.Pour(coctailInt, ageInt);

                else if (ageInt < 18)
                    _mojito1.Pour(coctailInt, ageInt);
            }

            else if (coctailInt == 2)
            {
                if (ageInt >= 18)
                    _pinacolada2.Pour(coctailInt, ageInt);

                else if (ageInt < 18)
                    _pinacolada1.Pour(coctailInt, ageInt);
            }

        }
    }
}

