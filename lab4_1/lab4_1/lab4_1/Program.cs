﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4_1
{
    abstract class State
    {
        protected string strStatename;
        abstract public void Invite();
    }
    class RegularCustomer : State
    {
        public RegularCustomer()
        {
            strStatename = "Regular Custopmer";
        }
        override public void Invite()
        {
            Console.WriteLine("Plz come at our seminar");

        }
    }
    class SuperRegularCustomer : State
    {
        public SuperRegularCustomer()
        {
            strStatename = "Super Regular Customer";
        }
        override public void Invite()
        {
            Console.WriteLine("Plz come at our seminar");
            Console.WriteLine("Plz come at our closed show");
        }
    }
    class SpecialGuest : State
    {
        public SpecialGuest()
        {
            strStatename = "SpecialGuest";
        }
        override public void Invite()
        {
            Console.WriteLine("Plz come at our seminar");
            Console.WriteLine("Plz come at our closed show");
            Console.WriteLine("Plz come at our special event");

        }
    }



    class contextCustomer
    {
        public enum CustomerStateSetting
        {
            RegularCustomer,
            SuperRegularCustomer,
            SpecialGuest
        };
        RegularCustomer RegularCustomer = new RegularCustomer();
        SuperRegularCustomer SuperRegularCustomer = new SuperRegularCustomer();
        SpecialGuest SpecialGuest = new SpecialGuest();
        public contextCustomer()
        {
            // Initialize to closed
            CurrentState = SuperRegularCustomer;
        }
        private State CurrentState;
        public void SetState(int numberVisits)
        {
            if (numberVisits >=3 && numberVisits <= 5)
            {
                CurrentState = RegularCustomer;
            }
            if (numberVisits >= 5 && numberVisits <= 7)
            {
                CurrentState = SuperRegularCustomer;
            }
            if (numberVisits >= 7 )
            {
                CurrentState = SpecialGuest;
            }
        }
        public void Invite()
        {
            CurrentState.Invite();
        }
    }
    public class Client
    {
        public static int Main(string[] args)
        {
            contextCustomer contextCustomer = new contextCustomer();
            Console.WriteLine("Now trying to Invite");
            contextCustomer.Invite();
            Console.WriteLine("Invited");
            Console.WriteLine("Try to Invite again");
            contextCustomer.Invite();
            Console.ReadKey();
            return 0;
        }
    }
}

