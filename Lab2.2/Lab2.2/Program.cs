﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Player player1 = new Player("Stas");
            Ship1 ship1 = new Ship1();
            Ship2 ship2 = new Ship2();
            player1.addShip(ship1, 1, 1, "horizontal");
            player1.field.displayField();
            player1.addShip(ship2, 0, 0, "horizontal");
            player1.field.displayField();
            player1.addShip(ship1, 3, 3, "horizontal");
            player1.addShip(ship2, -1, 3, "horizontal");
            player1.field.displayField();
            player1.addShip(ship1, 5, 5, "horizontal");
            player1.field.displayField();
            player1.addShip(ship1, 7, 7, "horizontal");
            player1.addShip(ship1, 9, 9, "horizontal");
            player1.field.displayField();
            Console.ReadKey();
        }
    }
}
