﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2._2
{
    abstract class Ship
    {
        public int length;
    }

    class Ship1 : Ship
    {
        public Ship1()
        {
            length = 1;
        }

    }

    class Ship2 : Ship
    {
        public Ship2()
        {
            length = 2;
        }

    }

    class Ship3 : Ship
    {
        public Ship3()
        {
            length = 3;
        }

    }

    class Ship4 : Ship
    {
        public Ship4()
        {
            length = 4;
        }
    }
}
