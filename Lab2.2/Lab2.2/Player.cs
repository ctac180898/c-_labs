﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2._2
{
    class Player
    {
        int ship1count, ship2count, ship3count, ship4count;
        string name;
        public Field field;

        public Player(string name)
        {
            ship1count = 4;
            ship2count = 3;
            ship3count = 2;
            ship4count = 1;
            field = new Field();
            this.name = name;
        }

        public bool isEnoughShips(Ship ship)
        {
            if(ship is Ship1 && ship1count > 0)
            {
                ship1count--;
                return true;
            }
            else if (ship is Ship2 && ship2count > 0)
            {
                ship2count--;
                return true;
            }
            else if (ship is Ship3 && ship3count > 0)
            {
                ship3count--;
                return true;
            }
            else if (ship is Ship4 && ship4count > 0)
            {
                ship4count--;
                return true;
            }
            return false;
        }

        public void addShip(Ship ship, int x, int y, string rotation)
        {
            if (isEnoughShips(ship))
                field.addShip(ship, x, y, rotation);
            else
                Console.WriteLine("Not enough ships");
        }
    }
}
