﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2._2
{
    class Field
    {
        public List<Coord> coords;

        public Field()
        {
            coords = new List<Coord>();
        }

        public bool checkField(Ship ship, int x, int y, string rotation)
        {
            int xT = 0, yT = 0;

            foreach (Coord coord in coords)
            {
                if (coord.rotation == "horizontal")
                    xT = coord.x;
                else
                    yT = coord.y;
                for (int j = 0; j < ship.length; j++)
                {
                    if (coord.rotation == "horizontal")
                        x++;
                    else
                        y++;
                    for (int i = 0; i < coord.ship.length; i++)
                    {
                        if (coord.rotation == "horizontal")
                            xT++;
                        else
                            yT++;
                        if ((xT == x && yT == y) 
                            || (xT + 1 == x && yT + 1 == y) 
                            || (xT - 1 == x && yT - 1 == y) 
                            || (xT - 1 == x && yT + 1 == y) 
                            || (xT + 1 == x && yT - 1 == y)
                            || (xT == x && yT + 1 == y)
                            || (xT + 1 == x && yT == y) 
                            || (xT - 1 == x && yT == y)
                            || (xT == x && yT - 1 == y))
                            return false;
                    }
                }    
            }

            return true;
        }

        public bool checkData(int x, int y, string rotation)
        {
            if((rotation == "horizontal" || rotation == "vertical") && x > -1 && y > -1 && x < 10 && y < 10)
                return true;
            return false;
        }

        public void addShip(Ship ship, int x, int y, string rotation)
        {

            if (!checkData(x, y, rotation))
            {
                Console.WriteLine("Inavlid data input");
                return;
            }

            if(checkField(ship, x, y, rotation))
                coords.Add(new Coord(x, y, rotation, ship));
            else
            {
                Console.WriteLine("Invalid coordinate x: {0} y: {1}", x, y);
            }
        }

        public void displayField()
        {
            Console.WriteLine("Field: ");
            foreach (Coord coord in coords){
                coord.print();
            }
        }
    }

    class Coord
    {
        public int x, y;
        public Ship ship;
        public string rotation;
        public Coord(int x, int y, string rotation, Ship ship)
        {
            this.x = x;
            this.y = y;
            this.rotation = rotation;
            this.ship = ship;
        }

        public void print()
        {
            Console.WriteLine("Ship's length: {0} Coords: {1} {2} Rotation: {3}", ship.length, x, y, rotation);
        }
    }
}
