﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    abstract class AbstMarks
    {
        protected Dictionary<string, int> marks;

        public abstract void addMark(User user, string name, int mark);
    }
}
