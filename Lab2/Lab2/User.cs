﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class User
    {
        public bool isAdmin;
        public string name;

        public User(bool isAdmin, string name)
        {
            this.isAdmin = isAdmin;
            this.name = name;
        }

        public bool IsAdmin
        {
            get { return isAdmin; }
        }
    }
}
