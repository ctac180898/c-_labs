﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Marks : AbstMarks
    {
        public Marks()
        {
            marks = new Dictionary<string, int>();
        }
       override
        public void addMark(User user, string name, int mark)
        {
            marks.Add(name, mark);
            Console.WriteLine("added");
        }

        public void displayMarks()
        {
            foreach (Object mark in marks)
                Console.WriteLine(mark.ToString());
        }
    }
}
