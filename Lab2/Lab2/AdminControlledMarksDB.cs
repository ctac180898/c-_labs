﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class AdminControlledMarksDB : AbstMarks
    {
        private Marks marks = new Marks();

        override
        public void addMark(User user, string name, int mark)
        {
            if (user.IsAdmin)
            {
                marks.addMark(user, name, mark);
            }
            else
            {
                Console.WriteLine("Permission denied");
            }
        }

        public void displayMarks()
        {
            marks.displayMarks();
        }

    }
}
