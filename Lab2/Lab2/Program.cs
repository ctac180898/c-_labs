﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            User Stas = new User(true, "Stas");
            User Potato = new User(false, "Potato");
            AdminControlledMarksDB marks = new AdminControlledMarksDB();
            marks.addMark(Stas, "Anton", 2);
            marks.addMark(Potato, "Anton", 2);
            marks.displayMarks();
            Console.ReadKey();
        }
    }
}
