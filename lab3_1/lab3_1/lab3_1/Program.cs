﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPatternExample
{
    interface ICard
    {
        string CardType();
    }

    class CardA : ICard
    {
        public String CardType()
        {
            return " student card";
        }
    }

    class CardB : ICard
    {
        public String CardType()
        {
            return "science card";
        }
    }

    class CardC : ICard
    {
        public String CardType()
        {
            return "special card for BOOK LOVER";
        }
    }

    class Creator
    {
        public ICard FactoryMethod(int type)
        {
            if (type == 1)
                return new CardA();
            else if (type == 2)
                return new CardB();
            else
                return new CardC();
        }
    }

    class Program
    {
        static void Main()
        {

            Creator c = new Creator();
            ICard card;
            for (int i = 1; i <= 3; i++)
            {
                card = c.FactoryMethod(i);
                Console.WriteLine("Library " + card.CardType());
            }
            Console.ReadKey();
        }
    }
}