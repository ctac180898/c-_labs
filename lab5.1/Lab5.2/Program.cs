﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5._2
{
    class Program
    {
        static void Main(string[] args)
        {

            Secretary secretary = new Secretary();

            Worker Stas = new Manager("Stas");
            Worker Toha = new Manager("Toha");
            Worker Tosha = new Manager("Tosha");
            Worker Vlad = new Manager("Vlad");

            Worker Vasya = new CommonWorker("Vasya");
            Worker Yura = new CommonWorker("Yura");
            Worker Kolya = new CommonWorker("Kolya");
            Worker Lyoha = new CommonWorker("Lyoha");
            Worker Oleg = new CommonWorker("Oleg");
            Worker Slava = new CommonWorker("Slava");

            secretary.Register(Stas);
            secretary.Register(Toha);
            secretary.Register(Tosha);
            secretary.Register(Vlad);
            secretary.Register(Vasya);
            secretary.Register(Yura);
            secretary.Register(Kolya);
            secretary.Register(Lyoha);
            secretary.Register(Oleg);
            secretary.Register(Slava);

            secretary.CallWorkersForMeeting();
            Console.ReadKey();

        }
    }
}
