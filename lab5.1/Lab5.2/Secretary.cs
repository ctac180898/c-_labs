﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5._2
{
    // The 'Mediator' abstract class

    abstract class AbstractSecretary
    {
        public abstract void Register(Worker participant);
        public abstract void CallWorkersForMeeting();
        public abstract void CallManagersForMeeting();
        public abstract void ReceiveConfirmation(string name);
    }

    class Secretary : AbstractSecretary
    {
        private List<Worker> _workers = new List<Worker>();

        public override void Register(Worker worker)
        {
            worker.Secretary = this;
            _workers.Add( worker);
        }

        public override void CallWorkersForMeeting()
        {
            foreach( Worker w in _workers)
            {
                w.GoOnAllWorkersMeeting();
            }
        }

        public override void CallManagersForMeeting()
        {
            foreach (Worker w in _workers)
            {
                if (!(w is CommonWorker))
                    w.GoOnManagersMeeting();
            }
        }

        public override void ReceiveConfirmation(string name)
        {
            Console.WriteLine("{0} confirmed his participation in meeting", name);
        }

    }

    class Worker
    {
        private Secretary _secretary;
        private string _name;
        // Constructor
        public Worker(string name)
        {
            this._name = name;
        }

        // Gets participant name
        public string Name
        {
            get { return _name; }
        }

        public Secretary Secretary
        {
            set { _secretary = value; }
            get { return _secretary; }
        }

        public virtual void GoOnAllWorkersMeeting()
        {
            Console.WriteLine("{0} will go on all workers meeting", Name);
        }

        public virtual void GoOnManagersMeeting()
        {
            Console.WriteLine("{0} will go on manager's meeting", Name);
        }
    }

    // A 'ConcreteColleague' class
    class Manager : Worker
    {
        // Constructor
        public Manager(string name) : base(name){ }

        public override void GoOnAllWorkersMeeting()
        {
            Console.WriteLine("Manager receives a message about all workers meeting");
            Secretary.ReceiveConfirmation(Name);
        }

        public override void GoOnManagersMeeting()
        {
            Console.WriteLine("Manager receives a message about manager's meeting", Name);
            Secretary.ReceiveConfirmation(Name);
        }

    }

    // A 'ConcreteColleague' class
    class CommonWorker : Worker
    {
        // Constructor
        public CommonWorker(string name) : base(name) { }

        public override void GoOnAllWorkersMeeting()
        {
            Console.WriteLine("Common Worker receives a message about all workers meeting");
            Secretary.ReceiveConfirmation(Name);
        }
    }
}
