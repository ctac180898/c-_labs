﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab5
{
    public interface IHandler
    {
        HandlerBase _atm { set; }
    }


    abstract public class HandlerBase
    {
        protected HandlerBase _atm;
        public HandlerBase Atm
        {
            set { _atm = value; }
        }
        abstract public string CalculateBills(int cash);
    }

    public class ConcreteHandler1 : HandlerBase
    {
        override public string CalculateBills(int cash)
        {

            if (cash % 200 == 0)
            {
                return String.Format("You`ve got {0} 200-hrn bills", cash / 200);
            }


            else
            {
                if (_atm != null)
                {
                    return _atm.CalculateBills(cash);
                }
                else
                    throw new ApplicationException("ChainOfResponsibility object exhausted all successors without call being handled.");
            }
        }
    }

    public class ConcreteHandler2 : HandlerBase
    {
        override public string CalculateBills(int cash)
        {

            if (cash % 100 == 0)
            {
                return String.Format("You`ve got {0} 100-hrn bills", cash / 100);
            }


            else
            {
                if (_atm != null)
                {
                    return _atm.CalculateBills(cash);
                }
                else
                    throw new ApplicationException("ChainOfResponsibility object exhausted all successors without call being handled.");
            }
        }
    }

    public class ConcreteHandler3 : HandlerBase
    {
        override public string CalculateBills(int cash)
        {
            return String.Format("Sorry, atm doesnt have bills to issue {0} hrn. Please, enter amount of money, that is divisible by 100", cash);
        }
    }
}
