﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab5
{
    class Program
    {
        static void Main(string[] args)
        {

            HandlerBase chain = new ConcreteHandler3();
            HandlerBase more = new ConcreteHandler2();
            more.Atm = chain;
            chain = new ConcreteHandler1();
            chain.Atm = more;

            int cash = -1;

            while (cash != 0)
            {
                Console.WriteLine("Enter amount of money, or 0 to exit");
                string scash = Console.ReadLine();

                if(scash != "")
                {
                    cash = Int32.Parse(scash);
                    Console.WriteLine(chain.CalculateBills(cash));
                }
            }
        }
    }
}
